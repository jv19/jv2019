/** 
Proyecto: Juego de la vida.
 * Organiza y gestiona la configuración de la aplicación.
 * Utiliza Properties para organizar y almacenar la configuración de manera persistente en un fichero.
 * Aplica una variante del patrón Singleton.
 * @since: prototipo 0.2.0
 * @source: Configuracion.java 
 * @version: 0.2.0 - 2020/05/15
 * @author: JGR, JJMZ, TIP, FGCB
 * @author: ajp - Grupo 0
 */

package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Configuracion {

	// Singleton.
	private static Properties configuracion;
	private File fConfig;
	
	/**
	 *  Método estático de acceso a la instancia única.
	 *  Si no existe la crea invocando al constructor privado.
	 *  Utiliza inicialización diferida.
	 *  Sólo se crea una vez; instancia única -patrón singleton-
	 *  @return instancia
	 */
	public static Properties get() {
		if (configuracion == null) {
			new Configuracion();
		}
		return configuracion;
	}
	
	/**
	 * Constructor por defecto de uso interno.
	 */
	private Configuracion() {
		configuracion = new Properties();
		fConfig = obtenerRutaFichero();
		try {
			if (fConfig.exists()) {
				InputStream is = new FileInputStream(fConfig);
				configuracion.load(is);    // Carga configuración desde el fichero.
				is.close();
			}
			else {
				cargarPredeterminados();   // La primera ejecución genera en fichero la config predeterminada.
				guardar();
			}
		} 
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private File obtenerRutaFichero() {
		new File("datos").mkdirs();         // nombre directorio.
		return new File("." + File.separator + "datos"	+ File.separator + "jv2019.cfg"); 
	}

	private void cargarPredeterminados() {
		configuracion.put("aplicacion.titulo", "JV2019");
		
		configuracion.put("mundo.nombrePredeterminado", "Demo0");
		configuracion.put("mundo.sizePredeterminado", "1");
		configuracion.put("mundo.tipoPredeterminado", "PLANO");
		configuracion.put("simulacion.ciclosPredeterminados", "35");
		configuracion.put("fecha.predeterminadaFija", "2002.02.02");
		
		configuracion.put("usuario.admin", "Admin");
		configuracion.put("usuario.adminId", "AAA1R");
		configuracion.put("usuario.adminNif", "00000001R");
		configuracion.put("usuario.adminClaveAcceso", "Miau#0");
		configuracion.put("usuario.invitado", "Invitado");
		configuracion.put("usuario.invitadoId", "III0T");
		configuracion.put("usuario.edadMinima", "16"); 
		configuracion.put("usuario.fechaNacimientoPredeterminada", "2001.01.01");
		
		configuracion.put("nif.predeterminado", "00000000T");
		configuracion.put("correo.dominioPredeterminado", "@gmail.com");
		configuracion.put("direccion.predeterminada", "Iglesia,0,30012,Murcia");
		configuracion.put("claveAcceso.predeterminada", "Miau#0");
		
		configuracion.put("sesion.intentosPermitidos", "3");
		
		configuracion.put("datos.nombreDirectorio", "datos");
		configuracion.put("datos.nombreFicheroUsuarios", "usuarios.dat");
		configuracion.put("datos.nombreFicheroEquivalenciasId", "equivalId.dat");
		configuracion.put("datos.nombreFicheroSesiones", "sesiones.dat");
		configuracion.put("datos.nombreFicheroSimulaciones", "simulaciones.dat");
		configuracion.put("datos.nombreFicheroMundos", "mundos.dat");
		
		configuracion.put("db4o.nombreFicheroDB", "JVdatos.db4o");
		configuracion.put("mySql.url", "jdbc:mysql://172.20.254.134/jvida?");
		configuracion.put("mySql.localhost", "jdbc:mysql://localhost/jvida?");
		configuracion.put("mySql.user", "admin");
		configuracion.put("mySql.passwd", "Miau#000");
	}

	/**
	 * Guarda configuración al fichero.
	 */
	public void guardar() {
		try {
			OutputStream os = new FileOutputStream(fConfig);
			configuracion.store(os, "Configuracion actualizada");
		} 
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}