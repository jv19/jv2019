/** Proyecto: Juego de la vida.
 *  Operaciones que deben estar disponibles para el inicio de sesión.
 *  @since: prototipo 0.2.0
 *  @source: OperacionesVistaSesion.java 
 *  @version: 0.2.0 - 2020.05.26
 *  @author: ajp - Grupo 0
 */

package accesoUsr.vista;

public interface OperacionesVistaInicioSesion extends OperacionesVista {

	/** 
	 *  Para interactuar con el usuario y que introduzca su identificador. 
	 * @return Devuelve el texto del identificador de usuario.
	 */
	String pedirIdUsr();

	/**
	 * Para interactuar con el usuario y que introduzca su contraseña. 
	 * @return Devuelve el texto de la contraseña.
	 */
	String pedirClaveAcceso();

}
