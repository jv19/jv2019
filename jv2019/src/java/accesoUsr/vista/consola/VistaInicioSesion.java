/** Proyecto: Juego de la vida.
 *  Resuelve todos los aspectos relacionados con la presentación del inicio de sesión de usuario.
 *  Es una concreción tecnológica basada en consola.
 *  Colabora en el patrón MVC, en realidad la variante MVP (presentador).
 *  @since: prototipo 0.2.0
 *  @source: VistaInicioSesion.java 
 *  @version: 0.2.0 - 2020.05.26
 *  @author: ajp - Grupo 0
 */

package accesoUsr.vista.consola;

import java.io.Console;
import java.util.Scanner;

import accesoUsr.vista.OperacionesVistaInicioSesion;

public class VistaInicioSesion implements OperacionesVistaInicioSesion {

private Console consola;
	
	public VistaInicioSesion() {
		consola = System.console();
	}

	@Override
	public void mostrarMensaje(String mensaje) {
		if (consola != null) {
			consola.writer().println(mensaje);
			return;
		}
		System.out.println(mensaje);	
	}

	@Override
	public String pedirIdUsr() {
		mostrarMensaje("Introduce clave acceso: ");
		if (consola != null) {
			return new String(consola.readPassword());
		}
		// Desde entorno Eclipse la consola falla.
		return new Scanner(System.in).nextLine();
	}

	@Override
	public String pedirClaveAcceso() {
		mostrarMensaje("Introduce clave acceso: ");
		if (consola != null) {
			return new String(consola.readPassword());
		}
		// Desde entorno Eclipse la consola falla.
		return new Scanner(System.in).nextLine();
	}
}
