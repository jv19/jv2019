/** Proyecto: Juego de la vida.
 *  Operaciones que deben estar disponibles en todas las clases vista.
 *  @since: prototipo 0.2.1
 *  @source: OperacionesVista.java 
 *  @version: 0.2.0 - 2020.05.26
 *  @author: ajp - Grupo 0
 */

package accesoUsr.vista;

public interface OperacionesVista {

	/**
	 *  Muestra un mensaje en el interfaz de usuario con el texto recibido.
	 */
	void mostrarMensaje(String mensaje);

}
