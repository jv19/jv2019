/** Proyecto: Juego de la vida.
 *  Resuelve todos los aspectos relacionados con el estado, sincronización 
 *  y lógica de presentación principal del programa con un menú. 
 *  Colabora en el patrón MVC, en realidad la variante MVP (presentador).
 *  @since: prototipo 0.2.0
 *  @source: ControlPrincipal.java 
 *  @version: 0.2.0 - 2020.06.01
 *  @author: ajp
 */

package accesoUsr.control.consola;

import accesoDatos.DatosFacade;
import accesoUsr.vista.consola.VistaPrincipal;
import modelo.Sesion;
import modelo.Simulacion;

public class ControlPrincipal {

	private VistaPrincipal vistaPrincipal;
	private Sesion sesionUsr;
	private DatosFacade datos;
	
	public ControlPrincipal() {
		this(null);
	}
	
	public ControlPrincipal(String idUsr) {
		initMenuPrincipal(idUsr);	
	}

	private void initMenuPrincipal(String idUsr) {
		datos = new DatosFacade();
		vistaPrincipal = new VistaPrincipal();
		sesionUsr = new ControlInicioSesion(idUsr).getSesion();
		secuenciaPrincipal();
	}

	private void secuenciaPrincipal() {
		do {
			vistaPrincipal.mostrar();
			vistaPrincipal.pedirOpcion();
			procesarOpcion();	
		} while (true);
	}

	private void procesarOpcion() {
		switch (vistaPrincipal.getOpcionActiva()) {
		case 0:
			salir();

			// Simulaciones
		case 1:
			crearNuevaSimulacion();
			break;
		case 2:
			modificarSimulacion();
			break;
		case 3:
			eliminarSimulacion();
			break;
		case 4:
			mostrarSimulaciones();
			break;
		case 5:
			mostrarIdSimulaciones();
			break;
		case 6:
			ejecutarDemoSimulacion();
			break;

			// Mundos
		case 7:
			crearNuevoMundo();
			break;
		case 8:
			modificarMundo();
			break;
		case 9:
			eliminarMundo();
			break;
		case 10:
			mostrarMundos();
			break;

			// Usuarios
		case 11:
			crearNuevoUsuario();
			break;
		case 12:
			modificarUsuario();
			break;
		case 13:
			eliminarUsuario();
			break;
		case 14:
			mostrarUsuarios();
			break;

			// Sesiones
		case 15:
			modificarSesion();
			break;
		case 16:
			eliminarSesion();
			break;
		case 17:
			mostrarSesiones();
			break;
		case 18:
			mostrarIdSesiones();
			break;
		default:
			vistaPrincipal.mostrarMensaje("Opción incorrecta...");
			break;
		}	

	}

	private void mostrarIdSesiones() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void mostrarSesiones() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void eliminarSesion() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void modificarSesion() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void mostrarUsuarios() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void eliminarUsuario() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void modificarUsuario() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void crearNuevoUsuario() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void mostrarMundos() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void eliminarMundo() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void modificarMundo() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void crearNuevoMundo() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void ejecutarDemoSimulacion() {
		new ControlEjecucionSimulacion(new Simulacion());
		
	}

	private void mostrarIdSimulaciones() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void mostrarSimulaciones() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void eliminarSimulacion() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void modificarSimulacion() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void crearNuevaSimulacion() {
		vistaPrincipal.mostrarMensaje("Opción no disponible...");
		
	}

	private void salir() {
		datos.cerrar();
		vistaPrincipal.mostrarMensaje("\nFin de programa...");	
		System.exit(1); 
	}
	
}
