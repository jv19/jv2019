/** Proyecto: Juego de la vida.
 *  Resuelve todos los aspectos relacionados con el estado, 
 *  sincronización y lógica de presentación del inicio de sesión de usuario.
 *  Colabora en el patrón MVC, en realidad la variante MVP (presentador).
 *  @since: prototipo 0.2.0
 *  @source: ControlInicioSesion.java 
 *  @version: 0.2.0 - 2020.05.26
 *  @author: ajp - Grupo 0
 */

package accesoUsr.control.consola;

import accesoDatos.DatosFacade;
import accesoUsr.AccesoUsrException;
import accesoUsr.vista.consola.VistaInicioSesion;
import config.Configuracion;
import modelo.Sesion;
import modelo.Usuario;
import util.Criptografia;

public class ControlInicioSesion {

	private VistaInicioSesion vistaSesion;
	private Sesion sesion;
	private DatosFacade datos;
	
	public ControlInicioSesion() {
		this(null);
	}

	public ControlInicioSesion(String idUsr) {
		initControlSesion(idUsr);
	}

	private void initControlSesion(String idUsr) {
		datos = new DatosFacade();
		vistaSesion = new VistaInicioSesion();
		vistaSesion.mostrarMensaje(Configuracion.get().getProperty("aplicacion.titulo"));
		iniciarSesionUsuario(idUsr);
	}

	/**
	 * Controla el acceso de usuario 
	 * y registro de la sesión correspondiente.
	 * @param idUsr ya obtenido, puede ser null.
	 */
	private void iniciarSesionUsuario(String idUsr) {
		int intentosPermitidos = Integer.parseInt(Configuracion.get().getProperty("sesion.intentosPermitidos"));
		String id = idUsr;
		do {
			if (id == null) {
				// Pide credencial usuario si llega null.
				id = vistaSesion.pedirIdUsr();	
			}
			id = id.toUpperCase();
			vistaSesion.mostrarMensaje("Usuario id: " + id);
			String clave = vistaSesion.pedirClaveAcceso();		
			try {
				validarCredenciales(id, clave);
				return;	
			} 
			catch (AccesoUsrException e) {
				intentosPermitidos--;
				id = null;
				vistaSesion.mostrarMensaje("Credenciales incorrectas...");
				vistaSesion.mostrarMensaje("Quedan " + intentosPermitidos + " intentos... ");
			}
		}
		while (intentosPermitidos > 0);
		vistaSesion.mostrarMensaje("Fin del programa...");
		datos.cerrar();
		System.exit(0);	
	}

	/**
	 * Valida las credenciales de usuario consultando en el almacén de datos.
	 * @param id
	 * @param clave
	 * @thows AccesoUsrException si las credenciales son incorrectas.
	 */
	private void validarCredenciales(String id, String clave) {

		// Obtiene del almacén el usuario a partir de su id.
		Usuario usrEnSesion = datos.consultarUsuario(id);
		if (usrEnSesion != null 
				&& usrEnSesion.getClaveAcceso().getTexto().equals(Criptografia.cesar(clave))) {
			sesion = new Sesion(usrEnSesion);
			datos.altaSesion(sesion);
			return;
		} 
		throw new AccesoUsrException("Presentacion.validarCredenciales: Credenciales incorrectas... ");
	}

	public Sesion getSesion() {
		return sesion;
	}

}
