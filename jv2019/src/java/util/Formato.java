/** 
 * Proyecto: Juego de la vida.
 *  Clase-utilidades de validación de formatos utilizando regex.
 *  @since: prototipo 0.1.1
 *  @source: Formato.java 
 *  @version: 0.1.2 - 2020/03/07
 *  @author: ajp
 */

package util;

import java.util.regex.Pattern;

public class Formato {

	public static final String PATRON_CORREO = "^[\\w-\\+]+(\\.[\\w-\\+]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static final String PATRON_PASSWORD = "[A-ZÑa-zñ0-9!?$%&#]{6,18}";

	public static final String LETRAS_NIF = "TRWAGMYFPDXBNJZSQVHLCKE";
	public static final String PATRON_NIF = "^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]";
		
	public static final String PATRON_CP = "^[\\d]{5}";
	public static final String PATRON_NUMERO_POSTAL = "[\\d]+[\\w]?";

	public static final String PATRON_NOMBRE_PERSONA = "^[A-ZÑ][áéíóúña-z ]+";
	public static final String PATRON_APELLIDOS = "^[A-ZÑ][áéíóúña-z ]+[ A-ZÑa-zñáéíóú]*";
	public static final String PATRON_TOPONIMO = "^[A-ZÑ][áéíóúña-z ]+";
	public static final String PATRON_NOMBRE_VIA = "^[A-ZÑ][/áéíóúña-z ]+";

	public static final String PATRON_ALFANUMERICO = "^[A-Z][a-z]+[A-Za-z_0-9]*";
	
	/**
	 * Verifica que un texto tiene un formato válido.
	 * @param texto - a validar.
	 * @param patron - a utilizar.
	 * @return - true si es correcto.
	 */
	public static boolean validar(String texto,  String patron) {
		return Pattern.compile(patron).matcher(texto).matches();
	}
	
} 