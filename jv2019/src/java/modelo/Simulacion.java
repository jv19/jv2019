/** 
 * Proyecto: Juego de la vida.
 * Organiza aspectos de gestión de la simulación según el modelo 2 
 * @since: prototipo 0.1.0
 * @source: Simulacion.java 
 * @version: 0.2.0 - 2020.06.01
 * @author: ajp - Grupo 0
 */

package modelo;

import config.Configuracion;
import util.Fecha;

public class Simulacion implements Identificable {
	
	public enum EstadoSimulacion {
		PREPARADA,
		INICIADA,
		COMPLETADA
	}

	private Usuario usr;
	private Fecha fecha;
	private Mundo mundo;
	private int ciclosSimulacion;
	private EstadoSimulacion estado;

	/**
	 * Constructor convencional.
	 * Establece el valor inicial de cada uno de los atributos.
	 * Recibe parámetros que se corresponden con los atributos.
	 * Utiliza métodos set... para la posible verificación.
	 * @param usr
	 * @param fecha
	 * @param espacioMundo
	 * @param formaEspacio
	 */
	public Simulacion(Usuario usr, Fecha fecha, Mundo mundo) {
		setUsr(usr);
		setFecha(fecha);
		setMundo(mundo);
		setCiclosSimulacion(Integer.parseInt(Configuracion.get().getProperty("simulacion.ciclosPredeterminados")));
		this.estado = EstadoSimulacion.PREPARADA;
	}

	/**
	 * Constructor por defecto.
	 * Establece el valor inicial, por defecto, de cada uno de los atributos.
	 * Llama al constructor convencional de la propia clase. 
	 */
	public Simulacion()  {
		this(new Usuario(), 
				new Fecha(Configuracion.get().getProperty("fecha.predeterminadaFija")), 
				new Mundo()
				);	
	}

	/**
	 * Constructor copia.
	 * Establece el valor inicial de cada uno de los atributos a partir de
	 * los valores obtenidos de un objeto de su misma clase.
	 * El objeto Usuario es compartido (agregación).
	 * Llama al constructor convencional.
	 * @param simulacion - la Simulacion a clonar
	 */
	public Simulacion(Simulacion simulacion) {
		this(simulacion.usr, simulacion.fecha.clone(), new Mundo(simulacion.mundo));
		
		// El estado definido con el constructor convencional hay que actualizarlo.
		this.estado = simulacion.estado;
	}

	/**
	 * Sintetiza un identificador único concatenando el id de usuario y una marca de tiempo de 14 dígitos.
	 * @return el id de simulación generado.
	 */
	@Override
	public String getId() {
		return usr.getId() + "-" + fecha.toStringMarcaTiempo();
	}
	
	public Usuario getUsr() {
		return usr;
	}

	public void setUsr(Usuario usr) {
		if (usr != null) {
			this.usr = usr;
			return;
		}
		throw new ModeloException("Simulacion.usr: null...");
	}

	public Mundo getMundo() {
		return mundo;
	}
	
	public void setMundo(Mundo mundo) {
		assert mundo != null;
		this.mundo = mundo;
	}
	
	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		if (fecha != null) {
			this.fecha = fecha;
			return;
		}
		throw new ModeloException("Simulacion.fecha: null...");
	}

	public int getCiclosSimulacion() {
		return ciclosSimulacion;
	}
	
	public void setCiclosSimulacion(int ciclos) {
		// El mínimo de ciclos es 5
		if (ciclos > 4 && ciclos < Integer.MAX_VALUE) {
			this.ciclosSimulacion = ciclos;
			return;
		}
		throw new ModeloException("Simulacion.ciclos: fuera de rango...");
	}
	
	public EstadoSimulacion getEstado() {
		return estado;
	}

	@Override
	public String toString() {
		return String.format(
				"%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n",
						"Id:", getId(),
						"usuario:", usr.getId(),
						"fecha:", fecha,
						"mundo", mundo, 
						"estado", estado
				);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((mundo == null) ? 0 : mundo.hashCode());
		result = prime * result + ((usr == null) ? 0 : usr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Simulacion other = (Simulacion) obj;
		if (estado != other.estado)
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (mundo == null) {
			if (other.mundo != null)
				return false;
		} else if (!mundo.equals(other.mundo))
			return false;
		if (usr == null) {
			if (other.usr != null)
				return false;
		} else if (!usr.equals(other.usr))
			return false;
		return true;
	}

	@Override
	public Simulacion clone() {
		return new Simulacion(this);
	}
} 
