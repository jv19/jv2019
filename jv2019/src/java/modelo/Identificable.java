/** 
 * Proyecto: Juego de la vida.
 * Los objetos identificables poseen un id utilizado para la búsqueda 
 * y ordenación en estructuras con acceso por índice.
 * @since: prototipo0.1.2
 * @source: Identificable.java 
 * @version: 0.1.2 - 2020/02/18 
 * @author: ajp
 */

package modelo;

public interface Identificable {
	String getId();
}