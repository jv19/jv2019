/** Proyecto: Juego de la vida.
 *  Implementa el concepto de Mundo según el modelo 2
 *  @since: prototipo 0.1.2
 *  @source: Mundo.java 
 *  @version: 0.2.0 - 2020/05/10
 *  @author: ajp - Grupo 0
 */

package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.Formato;

public class Mundo implements Identificable, Serializable {

	public enum FormaEspacio {
		PLANO,
		ESFERICO;
	}

	private String nombre;
	private byte[][] espacio;
	private List<Posicion> distribucion;
	private Map<String,int[]> constantes;   // Determinan las leyes del mundo.
	private FormaEspacio tipoMundo;

	/**
	 * Constructor convencional.
	 * Establece el valor inicial de cada uno de los atributos.
	 * Recibe parámetros que se corresponden con los atributos.
	 * Utiliza métodos set... para la posible verificación.
	 * @param nombre
	 * @param fecha
	 * @param distribucion
	 * @param formaEspacio
	 */	
	public Mundo(String nombre, List<Posicion> distribucion, FormaEspacio tipoMundo) {
		setNombre(nombre);
		setDistribucion(distribucion);
		aplicarLeyesEstandar();
		this.tipoMundo = tipoMundo;
	}

	private void aplicarLeyesEstandar() {
		constantes = new HashMap<String,int[]>();
		constantes.put("constantesSobrevivir", new int[] {2, 3});
		constantes.put("constantesRenacer", new int[] {3});
	}

	public Mundo() {
		this("Demo0", 
				new ArrayList<Posicion>(),
				Mundo.FormaEspacio.PLANO
				);
		generarEspacioDemo();
	}

	private void generarEspacioDemo() {
		List<Posicion> demo0 = new ArrayList<Posicion>();
		demo0.add(new Posicion(2,5));
		demo0.add(new Posicion(3,4));
		demo0.add(new Posicion(3,6));
		demo0.add(new Posicion(4,7));
		demo0.add(new Posicion(4,12));
		demo0.add(new Posicion(4,13));
		demo0.add(new Posicion(4,14));
		demo0.add(new Posicion(5,5));
		demo0.add(new Posicion(5,6));
		demo0.add(new Posicion(5,7));
		demo0.add(new Posicion(8,9));
		demo0.add(new Posicion(8,10));
		demo0.add(new Posicion(8,11));
		demo0.add(new Posicion(9,9));
		demo0.add(new Posicion(9,11));
		demo0.add(new Posicion(10,9));
		demo0.add(new Posicion(10,10));
		demo0.add(new Posicion(10,11));
		demo0.add(new Posicion(11,5));
		demo0.add(new Posicion(11,6));
		demo0.add(new Posicion(12,5));
		demo0.add(new Posicion(12,6));
		setDistribucion(demo0);
	}

	public Mundo(Mundo mundo) {
		if (mundo != null) {
			setNombre(mundo.nombre);
			setDistribucion(mundo.distribucion);
			aplicarLeyesEstandar();
			this.tipoMundo = mundo.tipoMundo;
			return;
		}
		throw new ModeloException("Mundo.copia: null...");
	}

	@Override
	public String getId() {
		return nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public byte[][] getEspacio() {
		return espacio;
	}

	public List<Posicion> getDistribucion() {
		return distribucion;
	}

	public FormaEspacio getTipoMundo() {
		return tipoMundo;
	}

	public HashMap<String,int[]> getConstantes() {
		return (HashMap<String, int[]>) constantes;
	}

	private void setNombre(String nombre) {
		if (nombreValido(nombre)) {
			this.nombre = nombre;
			return;
		}
		throw new ModeloException("Mundo.nombre: null o formato no válido...");
	}

	/**
	 * Comprueba validez del nombre.
	 * @param nombre.
	 * @return true si cumple.
	 */
	private boolean nombreValido(String nombre) {
		return nombre != null && nombre.matches(Formato.PATRON_ALFANUMERICO);
	}

	public void setEspacio(byte[][] espacio) {
		if (espacio != null) {
			this.espacio = espacio;
			generarDistribucion();
			return;
		}
		throw new ModeloException("Mundo.espacio: null...");	
	}

	private void generarDistribucion() {
		assert espacio != null;
		List<Posicion> distribucion = new ArrayList<Posicion>();
		for (int i = 0; i < espacio.length; i++) {
			for (int j = 0; j < espacio[i].length; j++) {
				if (espacio[i][j] == 1) {
					distribucion.add(new Posicion(i,j));
				}
			}
		}
		this.distribucion = distribucion;
	}

	public void setDistribucion(List<Posicion> distribucion) {
		if (distribucion != null) {
			this.distribucion = distribucion;
			generarEspacio();
			return;
		}
		throw new ModeloException("Mundo.distribucion: null...");
	}

	private void generarEspacio() {
		assert distribucion != null;
		if (distribucion.size() == 0) {
			espacio = new byte[0][0];
			return;
		}
		int xyMax = mayorXY();
		espacio = new byte[xyMax+1][xyMax+1];
		for (Posicion posicion : distribucion) {
			espacio[posicion.getY()][posicion.getX()] = 1;
		}
	}

	private int mayorXY() {
		int xy = 0;
		for (Posicion pos : distribucion) {
			if (pos.mayorXY() > xy) {
				xy = pos.mayorXY();
			}
		}
		return xy;
	}

	public void setTipoMundo(FormaEspacio tipoMundo) {
		if (tipoMundo != null) {
			this.tipoMundo = tipoMundo;
			return;
		}
		throw new ModeloException("Mundo.tipoMundo: null...");
	}

	public void setConstantes(HashMap<String,int[]> constantes) {
		if (constantes != null) {
			this.constantes = constantes;
			return;
		}
		throw new ModeloException("Mundo.constantes: null...");
	}


	@Override
	public String toString() {
		return String.format(
				"%-21s %s\n"
				+ "%-21s %s\n"
				+ "%-21s %s\n"
				+ "%-21s %s\n"
				+ "%-21s %s\n",
				"id:", nombre,
				"espacio.length:", espacio.length,
				"distribucion.size():", distribucion.size(),
				"constantesSobrevivir:", Arrays.toString(constantes.get("constantesSobrevivir")),
				"constantesRenacer:", Arrays.toString(constantes.get("constantesRenacer"))
				);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((constantes == null) ? 0 : constantes.hashCode());
		result = prime * result + ((distribucion == null) ? 0 : distribucion.hashCode());
		result = prime * result + Arrays.deepHashCode(espacio);
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((tipoMundo == null) ? 0 : tipoMundo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mundo other = (Mundo) obj;
		if (constantes == null) {
			if (other.constantes != null)
				return false;
		} else if (!constantes.equals(other.constantes))
			return false;
		if (distribucion == null) {
			if (other.distribucion != null)
				return false;
		} else if (!distribucion.equals(other.distribucion))
			return false;
		if (!Arrays.deepEquals(espacio, other.espacio))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (tipoMundo != other.tipoMundo)
			return false;
		return true;
	}

	@Override
	public Mundo clone() {
			return new Mundo(this);
	}
	
	/**
	 * Actualiza el estado del Juego de la Vida.
	 * Actualiza según la configuración establecida para la forma del espacio.
	 */
	public void actualizarEstado() {
		if (tipoMundo == FormaEspacio.PLANO) {
			actualizarMundoPlano();
		}
		if (tipoMundo == FormaEspacio.ESFERICO) {
			actualizarMundoEsferico();
		}
	}

	/**
	 * Actualiza el estado almacenado del Juego de la Vida.
	 * Las celdas periféricas son adyacentes con las del lado opuesto.
	 * El mundo representado sería esférico cerrado sin límites para células de dos dimensiones.
	 */
	private void actualizarMundoEsferico()  {     					
		// Pendiente de implementar.
	}

	/**
	 * Actualiza el estado de cada celda almacenada del Juego de la Vida.
	 * Las celdas periféricas son los límites absolutos.
	 * El mundo representado sería plano, cerrado y con límites para células de dos dimensiones.
	 */
	private void actualizarMundoPlano()  {     					
		byte[][] nuevoEstado = new byte[espacio.length][espacio.length];

		for (int i = 0; i < espacio.length; i++) {
			for (int j = 0; j < espacio.length; j++) {
				int conteoVecinas = 0;							
				conteoVecinas += visitarCeldaNoroeste(i, j);		
				conteoVecinas += visitarCeldaNorte(i, j);			// 		NO | N | NE
				conteoVecinas += visitarCeldaNoreste(i, j);			//    	-----------
				conteoVecinas += visitarCeldaEste(i, j);			// 		 O | * | E
				conteoVecinas += visitarCeldaSureste(i, j);			// 	  	----------- 
				conteoVecinas += visitarCeldaSur(i, j); 			// 		SO | S | SE
				conteoVecinas += visitarCeldaSuroeste(i, j); 	  
				conteoVecinas += visitarCeldaOeste(i, j);		          			                                     	

				actualizarCelda(nuevoEstado, i, j, conteoVecinas);
			}
		}
		this.espacio = nuevoEstado;
		sincronizarDistribucion();
	}

	/**
	 * Actualiza el estado de cada celda almacenada del Juego de la Vida.
	 * Las celdas periféricas son los límites absolutos.
	 * El mundo representado sería plano, cerrado y con límites para células de dos dimensiones.
	 */
	private void sincronizarDistribucion() {
		List<Posicion> nuevaDistribucion = new ArrayList<Posicion>();
		for (int i = 0; i < espacio.length; i++) {
			for (int j = 0; j < espacio.length; j++) {
				if (espacio[i][j] == 1) {
					nuevaDistribucion.add(new Posicion(i, j));
				}
			}
			this.distribucion = nuevaDistribucion;
		}	
	}

	/**
	 * Aplica las leyes del mundo a la celda indicada dada la cantidad de células adyacentes vivas.
	 * @param nuevoEstado
	 * @param fila
	 * @param col
	 * @param conteoVecinas
	 */
	private void actualizarCelda(byte[][] nuevoEstado, int fila, int col, int conteoVecinas) {	

		for (int valor : constantes.get("constantesRenacer")) {
			if (conteoVecinas == valor) {										// Pasa a estar viva.
				nuevoEstado[fila][col] = 1;
				return;
			}
		}

		for (int valor : constantes.get("constantesSobrevivir")) {
			if (conteoVecinas == valor && espacio[fila][col] == 1) {		// Permanece viva, si lo estaba.
				nuevoEstado[fila][col] = 1;
				return;
			}
		}
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Oeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Oeste.
	 */
	private byte visitarCeldaOeste(int fila, int col) {
		if (col-1 >= 0) {
			return espacio[fila][col-1]; 			// Celda O.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Suroeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Suroeste.
	 */
	private byte visitarCeldaSuroeste(int fila, int col) {
		if (fila+1 < espacio.length && col-1 >= 0) {
			return espacio[fila+1][col-1]; 		// Celda SO.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Sur de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Sur.
	 */
	private byte visitarCeldaSur(int fila, int col) {
		if (fila+1 < espacio.length) {
			return espacio[fila+1][col]; 			// Celda S.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Sureste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Sureste.
	 */
	private byte visitarCeldaSureste(int fila, int col) {
		if (fila+1 < espacio.length && col+1 < espacio.length) {
			return espacio[fila+1][col+1]; 		// Celda SE.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Este de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Este.
	 */
	private byte visitarCeldaEste(int fila, int col) {
		if (col+1 < espacio.length) {
			return espacio[fila][col+1]; 			// Celda E.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Noreste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Noreste.
	 */
	private byte visitarCeldaNoreste(int fila, int col) {
		if (fila-1 >= 0 && col+1 < espacio.length) {
			return espacio[fila-1][col+1]; 		// Celda NE.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al NO de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda NO.
	 */
	private byte visitarCeldaNorte(int fila, int col) {
		if (fila-1 >= 0) {
			return espacio[fila-1][col]; 		// Celda N.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Noroeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Noroeste.
	 */
	private byte visitarCeldaNoroeste(int fila, int col) {
		if (fila-1 >= 0 && col-1 >= 0) {
			return espacio[fila-1][col-1]; 		// Celda NO.
		}
		return 0;
	}

}
