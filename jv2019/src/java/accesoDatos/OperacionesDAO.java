/** 
 * Proyecto: Juego de la vida.
 * Interfaz con las operaciones estándar genéricas que debeb disponer todos los DAO.
 * @since: prototipo 0.2.0
 * @source: OperacionesDAO.java  
 * @version: 0.2.0 - 2020.04.20 
 * @author: ajp - Grupo 0
 */

package accesoDatos;

import java.util.List;

import modelo.Identificable;

public interface OperacionesDAO {

	/**
	 * Obtiene el objeto dado el id utilizado para el almacenamiento.
	 * @param id - el identificador del objeto a obtener.
	 * @return - el objeto encontrado; null si no existe.
	 */	
	Object consultar(String id);
	
	/**
	 * obtiene todos los objetos almacenados en una lista de identificables.
	 * @return - la lista.
	 */
	List<Identificable> consultarTodos();
	
	/**
	 *  Alta de un objeto en el almacén de datos, 
	 *  sin repeticiones, según el campo id previsto. 
	 *	@param obj - el objeto a almacenar.
	 *  @throws DatosException - si ya existe.
	 */
	void alta(Object obj) throws DatosException;
	
	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param id - el identificador del objeto a eliminar.
	 * @return - el Objeto eliminado.
	 * @throws DatosException - si no existe.
	 */
	Object baja(String id) throws DatosException;
	
	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 * @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	Object actualizar(Object obj) throws DatosException;
	
	/**
	 * Obtiene el listado de todos los datos almacenados.
	 * @return el texto con el volcado de datos.
	 */
	String toStringDatos();
	
	/**
	 * Obtiene el listado de todos id's de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	String toStringIds();
	
	/**
	 * Elimina todos los datos y restaura los valores predeterminados.
	 */
	void borrarTodo();
	
	/**
	 *  Cierra almacenes de datos.
	 *  No hace nada en la implementación por defecto.
	 */
	default void cerrar() {
	};
	
}
