/** 
 * Proyecto: Juego de la vida.
 * Almacén de datos del programa. Aplica el patrón Façade.
 * @since: prototipo 0.2.0
 * @source: DatosFacade.java 
 * @version: 0.2.0 - 2020.04.25
 * @author: ajp
 */

package accesoDatos;

import java.util.List;

import accesoDatos.memoria.*;
import modelo.Identificable;
import modelo.*;

public class DatosFacade {

	private UsuariosDAO usuariosDAO; 
	private SesionesDAO sesionesDAO;
	private SimulacionesDAO simulacionesDAO;
	private MundosDAO mundosDAO;

	/**
	 * Constructor por defecto.
	 */
	public DatosFacade() {
		usuariosDAO = UsuariosDAO.getInstance();
		sesionesDAO = SesionesDAO.getInstance();
		mundosDAO = MundosDAO.getInstance();
		simulacionesDAO = SimulacionesDAO.getInstance();
	}

	/**
	 *  Cierra almacen de datos.
	 */
	public void cerrar() {
		usuariosDAO.cerrar();
		sesionesDAO.cerrar();
		simulacionesDAO.cerrar();
		mundosDAO.cerrar();
	}

	// FACHADA usuariosDAO

	/**
	 * Método fachada que obtiene un Usuario dado el id. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el id de Usuario a obtener.
	 * @return - el Usuario encontrado; null si no encuentra.
	 */	
	public Usuario consultarUsuario(String id) {
		return usuariosDAO.consultar(id);
	}

	/**
	 * Método fachada que obtiene todos los objetos Usuario. 
	 * Reenvia petición al método DAO específico.
	 * @return - lista de objetos encontrados.
	 */	
	public List<Identificable> consultarTodosUsuario() {
		return usuariosDAO.consultarTodos();
	}

	/**
	 * Método fachada para alta de un Usuario. 
	 * Reenvia petición al método DAO específico.
	 * @param usuario - el objeto Usuario a dar de alta.
	 * @throws DatosException - si ya existe.
	 */
	public void altaUsuario(Usuario usuario) throws DatosException  {
		usuariosDAO.alta(usuario);
	}

	/**
	 * Método fachada para baja de un Usuario y sus dependencias. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el id de Usuario a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Usuario bajaUsuario(String id) throws DatosException {
		Usuario usrBaja = usuariosDAO.baja(id);
		bajaSesionesUsur(id);
		bajaSimulacionesUsr(id);
		return usrBaja;
	}

	private List<Identificable> bajaSimulacionesUsr(String idUsr) {
		// Baja de simulaciones dependientes.
		List<Identificable> simulacionesUsr = simulacionesDAO.consultarTodasUsuario(idUsr);
		if (simulacionesUsr != null) {
			for (Identificable simulBaja : simulacionesDAO.consultarTodasUsuario(idUsr)) {
				simulacionesDAO.baja(simulBaja.getId());
			}	
		}
		return simulacionesUsr;
	}

	private List<Identificable> bajaSesionesUsur(String idUsr) {
		// Baja de sesiones dependientes.
		List<Identificable> sesionesUsr = sesionesDAO.consultarTodasUsuario(idUsr);
		if (sesionesUsr != null) {
			for (Identificable sesionBaja : sesionesUsr) {
				sesionesDAO.baja(sesionBaja.getId());
			}
		}
		return sesionesUsr;
	}

	/**
	 * Método fachada para baja de un Usuario y sus dependencias. 
	 * @param usr - el objeto Usuario a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Usuario bajaUsuario(Usuario usr) throws DatosException {
		assert usr != null;
		return bajaUsuario(usr.getId());
	}

	/**
	 * Método fachada para modicar un Usuario. 
	 * Reenvia petición al método DAO específico.
	 * @param usr - el objeto Usuario con los cambios.
	 * @throws DatosException - si no existe.
	 */
	public void actualizarUsuario(Usuario usr) throws DatosException  {
		usuariosDAO.actualizar(usr);
	}

	/**
	 * Método fachada para obtener listado de todos
	 * los objetos en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringDatosUsuarios() {
		return usuariosDAO.toStringDatos();
	}

	/**
	 * Método fachada para obtener listado de todos los id's en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringIdsUsuarios() {
		return usuariosDAO.toStringIds();
	}

	/**
	 * Método fachada para eliminar todos los usuarios y sus dependencias.  
	 * Reenvia petición al método DAO específico.
	 */
	public void borrarTodosUsuarios() {
		usuariosDAO.borrarTodo();
		// Dependencias.
		sesionesDAO.borrarTodo();
		simulacionesDAO.borrarTodo();
	}

	// FACHADA sesionesDAO

	/**
	 * Método fachada que obtiene una Sesion dado el id. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el idUsr + fecha de la Sesion a obtener.
	 * @return - la Sesion encontrada; null si no encuentra.
	 */	
	public Sesion consultarSesion(String id) {
		return sesionesDAO.consultar(id);
	}

	/**
	 * Método fachada que obtiene todos los objetos Sesion. 
	 * Reenvia petición al método DAO específico.
	 * @return - lista de objetos encontrados.
	 */	
	public List<Identificable> consultarTodasSesiones() {
		return sesionesDAO.consultarTodos();
	}

	/**
	 * Método fachada que obtiene todas las sesiones de un usuario. 
	 * Reenvia petición al método DAO específico.
	 * @param idUsr - el id del usuario de las Sesiones a obtener.
	 * @return - lista de objetos encontrados.
	 */	
	public List<Identificable> consultarSesionesUsuario(String idUsr) {
		return sesionesDAO.consultarTodasUsuario(idUsr);
	}

	/**
	 * Método fachada para alta de una Sesion. 
	 * Reenvia petición al método DAO específico.
	 * @param sesion - el objeto a dar de alta.
	 * @throws DatosException - si ya existe.
	 */
	public void altaSesion(Sesion sesion) throws DatosException  {
		sesionesDAO.alta(sesion);
	}

	/**
	 * Método fachada para baja de una Sesion. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el idUsr + fecha de la Sesion a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Sesion bajaSesion(String id) throws DatosException  {
		return sesionesDAO.baja(id);
	}

	/**
	 * Método fachada para baja de una Sesion. 
	 * Reenvia petición al método DAO específico.
	 * @param sesion - el objeto a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Sesion bajaSesion(Sesion sesion) throws DatosException  {
		assert sesion != null;
		return sesionesDAO.baja(sesion.getId());
	}

	/**
	 * Método fachada para modicar una Sesión. 
	 * Reenvia petición al método DAO específico.
	 * @param sesion - el objeto SesionUsuario a modificar.
	 * @throws DatosException - si no existe.
	 */
	public void actualizarSesion(Sesion sesion) throws DatosException  {
		sesionesDAO.actualizar(sesion);
	}

	/**
	 * Método fachada para obtener listado de todos los objetos en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringDatosSesiones() {
		return sesionesDAO.toStringDatos();
	}

	/**
	 * Método fachada para obtener listado de todos los id's en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringIdsSesiones() {
		return sesionesDAO.toStringIds();
	}

	/**
	 * Método fachada para eliminar todas las sesiones.  
	 * Reenvia petición al método DAO específico.
	 */
	public void borrarTodasSesiones() {
		sesionesDAO.borrarTodo();
	}

	/**
	 * Método fachada para obtener total sesiones registradas.  
	 * Reenvia petición al método DAO específico.
	 */
	public int getSesionesRegistradas() {
		return sesionesDAO.consultarTodos().size();
	}

	// FACHADA simulacionesDAO

	/**
	 * Método fachada que obtiene una Simulacion dado el id. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el idUsr + fecha de la Simulacion a obtener.
	 * @return - la Simulacion encontrada; null si no encuentra.
	 */	
	public Simulacion consultarSimulacion(String id) {
		return simulacionesDAO.consultar(id);
	}

	/**
	 * Método fachada que obtiene todos los objetos Simulacion. 
	 * Reenvia petición al método DAO específico.
	 * @return - lista de objetos encontrados.
	 */	
	public List<Identificable> consultarTodasSimulacioness() {
		return simulacionesDAO.consultarTodos();
	}

	/**
	 * Método fachada que obtiene todas las simulaciones de un usuario. 
	 * Reenvia petición al método DAO específico.
	 * @param idUsr - el idUsr a obtener.
	 * @return - lista de simulaciones encontradas.
	 */	
	public List<Identificable> consultarSimulacionesUsuario(String idUsr) {
		return simulacionesDAO.consultarTodasUsuario(idUsr);
	}

	/**
	 * Método fachada para alta de una Simulacion. 
	 * Reenvia petición al método DAO específico.
	 * @param simulacion - el objeto a dar de alta.
	 * @throws DatosException - si ya existe.
	 */
	public void altaSimulacion(Simulacion simulacion) throws DatosException  {
		simulacionesDAO.alta(simulacion);
	}

	/**
	 * Método fachada para baja de una Simulacion dado su id. 
	 * Reenvia petición al método DAO específico.
	 * @param id - el idUsr + fecha de la Simulacion a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Simulacion bajaSimulacion(String id) throws DatosException  {
		return simulacionesDAO.baja(id);
	}

	/**
	 * Método fachada para baja de una Simulacion dado su id.
	 * Reenvia petición al método DAO específico.
	 * @param simulacion - el objeto a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Simulacion bajaSimulacion(Simulacion simulacion) throws DatosException  {
		assert simulacion != null;
		return simulacionesDAO.baja(simulacion.getId());
	}

	/**
	 * Método fachada para modicar una Simulacion. 
	 * Reenvia petición al método DAO específico.
	 * @param simulacion - el objeto a modificar.
	 * @throws DatosException - si no existe.
	 */
	public void actualizarSimulacion(Simulacion simulacion) throws DatosException  {
		simulacionesDAO.actualizar(simulacion);
	}

	/**
	 * Método fachada para obtener listado de todos
	 * los objetos en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringDatosSimulaciones() {
		return simulacionesDAO.toStringDatos();
	}

	/**
	 * Método fachada para obtener listado de todos los id's en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringIdsSimulaciones() {
		return simulacionesDAO.toStringIds();
	}

	/**
	 * Método fachada para eliminar todos las simulaciones.  
	 * Reenvia petición al método DAO específico.
	 */
	public void borrarTodasSimulaciones() {
		simulacionesDAO.borrarTodo();
	}

	// FACHADA mundosDAO

	/**
	 * Método fachada para obtener un dado su id. 
	 * Reenvia petición al método DAO específico.
	 * @param nombre - el id del objeto a buscar.
	 * @return - el Mundo encontrado null si no existe.
	 */
	public Mundo consultarMundo(String nombre) {
		assert nombre != null;
		return mundosDAO.consultar(nombre);
	}

	/**
	 * Método fachada que obtiene todos los objetos Mundo. 
	 * Reenvia petición al método DAO específico.
	 * @return - lista de objetos encontrados.
	 */	
	public List<Identificable> consultarTodosMundos() {
		return mundosDAO.consultarTodos();
	}

	/**
	 * Método fachada para alta de un Mundo. 
	 * Reenvia petición al método DAO específico.
	 * @param mundo - el objeto Mundo a dar de alta.
	 * @throws DatosException - si ya existe.
	 */
	public void altaMundo(Mundo mundo) throws DatosException  {
		mundosDAO.alta(mundo);
	}

	/**
	 * Método fachada para baja de un Mundo. 
	 * Reenvia petición al método DAO específico.
	 * @param id - del objeto a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Mundo bajaMundo(String id) throws DatosException  {
		return mundosDAO.baja(id);
	}

	/**
	 * Método fachada para baja de un Mundo.
	 * Reenvia petición al método DAO específico.
	 * @param nombre - el nombre de un Mundo a dar de baja.
	 * @throws DatosException - si no existe.
	 */
	public Mundo bajaMundo(Mundo mundo) throws DatosException  {
		assert mundo != null;
		return mundosDAO.baja(mundo.getId());
	}

	/**
	 * Método fachada para modicar un Mundo. 
	 * Reenvia petición al método DAO específico.
	 * @param mundo - el objeto Mundo a modificar.
	 * @throws DatosException - si no existe.
	 */
	public void actualizarMundo(Mundo mundo) throws DatosException   {
		mundosDAO.actualizar(mundo);
	}

	/**
	 * Método fachada para obtener listado de todos
	 * los objetos en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringDatosMundos() {
		return mundosDAO.toStringDatos();
	}

	/**
	 * Método fachada para obtener listado de todos los id's en forma de texto.  
	 * Reenvia petición al método DAO específico.
	 * @return - el texto.
	 */
	public String toStringIdsMundos() {
		return mundosDAO.toStringIds();
	}

	/**
	 * Método fachada para eliminar todos
	 * los mundos.  
	 * Reenvia petición al método DAO específico.
	 */
	public void borrarTodosMundos() {
		mundosDAO.borrarTodo();
	}

} //class