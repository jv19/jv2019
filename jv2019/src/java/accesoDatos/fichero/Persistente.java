/** 
 * Proyecto: Juego de la vida.
 * Interfaz con las operaciones básicas de persistencia en fichero.
 * @since: prototipo 0.2.0
 * @source: Persistente.java  
 * @version: 0.2.0 - 2020.05.15 
 * @author: ajp - Grupo 0
 */

package accesoDatos.fichero;

public interface Persistente {

	/**
	 *  Permite guarda el Arraylist de objetos en ficheros. 
	 */
	void guardarDatos();
	
	/**
	 *  Permite recupera el Arraylist de objetos almacenados en fichero. 
	 */
	void recuperarDatos();
}
