/** 
Proyecto: Juego de la vida.
 * Almacén de datos del programa. Utiliza patrón Façade.
 * @since: prototipo 0.1.1
 * @source: MundosDAO.java 
 * @version: 0.1.0 - 2020/04/23
 * @author: ENG
 */


package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Mundo;

public class MundosDAO extends IndexSortTemplate implements OperacionesDAO {

	// Singleton.
	private static MundosDAO instance;

	// Elementos de almacenamiento.
	private ArrayList<Identificable> datosMundos;

	/**
	 *  Método estático de acceso a la instancia única.
	 *  Si no existe la crea invocando al constructor interno.
	 *  Utiliza inicialización diferida.
	 *  Sólo se crea una vez; instancia única -patrón singleton-
	 *  @return instancia
	 */
	public static MundosDAO getInstance() {
		if (instance == null) {
			instance = new MundosDAO();
		}
		return instance;
	}

	/**
	 * Constructor por defecto de uso interno.
	 * Sólo se ejecutará una vez.
	 */
	private MundosDAO() {
		datosMundos = new ArrayList <Identificable>();
		cargarMundoDemo();
	}

	private void cargarMundoDemo() {
		try {
			alta(new Mundo());
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		}
	}
	
	//OPERACIONES DAO
	
	/**
	 * Obtiene el objeto dado el id utilizado para el almacenamiento.
	 * @param id - id del mundo a obtener.
	 * @return - el Mundo encontrado; null si no encuentra.
	 */	
	@Override
	public Mundo consultar(String id) {
		assert id != null;
		int posicion = indexSort(datosMundos, id);			// En base 1
		if (posicion >= 0) {
			return (Mundo) datosMundos.get(posicion - 1);   // En base 0
		}
		return null;
	}

	/**
	 * obtiene todos los mundos en una lista.
	 * @return - la lista.
	 */
	@Override
	public List<Identificable> consultarTodos() {
		return datosMundos;
	}

	/**
	 *  Alta de un objeto en el almacén de datos, 
	 *  sin repeticiones, según el campo id previsto. 
	 *	@param obj - Objeto a almacenar.
	 * @throws DatosException - si ya existe.
	 */
	@Override
	public void alta(Object obj) throws DatosException  {
		assert obj != null;
		Mundo mundoNuevo = (Mundo) obj;										// Para conversión cast
		int posInsercion = indexSort(datosMundos, mundoNuevo.getId()); 
		if (posInsercion < 0) {
			datosMundos.add(Math.abs(posInsercion)-1, mundoNuevo); 			// Inserta la sesión en orden.
		}
		else {
			throw new DatosException("MundosDAO.alta: "+ mundoNuevo.getId() + " ya existe");
		}
	}

	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param nombre - el nombre del Mundo a eliminar.
	 * @return - el Mundo eliminado.
	 * @throws DatosException - si no existe.
	 */
	@Override
	public Mundo baja(String nombre) throws DatosException  {
		assert (nombre != null);
		int posicion = indexSort(datosMundos, nombre); 									// En base 1
		if (posicion > 0) {
			return (Mundo) datosMundos.remove(posicion - 1); 				// En base 0
		}
		else {
			throw new DatosException("MundosDAO.baja: "+ nombre + " no existe");
		}
	}

	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 *  @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	@Override
	public Mundo actualizar(Object obj) throws DatosException {
		assert obj != null;
		Mundo mundoActualizado = (Mundo) obj;							// Para conversión cast
		int index = indexSort(datosMundos, mundoActualizado.getId());
		if (index > 0) {
			Mundo aux = (Mundo) datosMundos.get(index-1);
			/// index es en base 1
			datosMundos.set(index-1, mundoActualizado);
			return aux;
		}
		throw new DatosException("MundosDAO.actualizar: la sesión no existe...");
	}

	/**
	 * Obtiene el listado de todos los objetos Mundo almacenados.
	 * @return el texto con el volcado de datos.
	 */
	@Override
	public String toStringDatos() {
		StringBuilder result = new StringBuilder();
		for (Identificable mundo: datosMundos) {
			result.append("\n" + mundo);
		}
		return result.toString();
	}

	/**
	 * Obtiene el listado de todos id de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	@Override
	public String toStringIds() {
		StringBuilder result = new StringBuilder();
		for (Identificable mundo: datosMundos) {
			result.append("\n" + mundo.getId());
		}
		return result.toString();
	}
	
	/**
	 * Elimina todos los mundos almacenados y regenera el demo predeterminado.
	 */
	@Override
	public void borrarTodo() {
		datosMundos.clear();
		cargarMundoDemo();	
	}

} 