/** 
 * Proyecto: Juego de la vida.
 * Template Method para los DAOs que requieren el método indexSort() para mantener ordenados los datos.
 * @since: prototipo 0.2.0
 * @source: IndexSortTemplate.java  
 * @version: 0.2.0 - 2020.04.24
 * @author: ajp - Grupo 0
 */

package accesoDatos.memoria;

import java.util.List;

import modelo.Identificable;

public abstract class IndexSortTemplate {
	
	/**
	 *  Obtiene por búsqueda binaria, la posición que ocupa, o ocuparía,  un objeto en la estructura.
	 *	@param Id - id del objeto a buscar.
	 * 	@param datos - Estructura de datos con elementos identificables (poseen un id utilizado para la busqueda y ordenación).
	 *	@return - la posición, en base 1, que ocupa un objeto o la que ocuparía (negativo).
	 */
	protected int indexSort(List<Identificable> datos, String id) {
		int inicio = 0;
		int fin = datos.size() - 1;
		while (inicio <= fin) {
			int medio = (inicio+fin) / 2;	
			int comparacion = datos.get(medio).getId().compareTo(id);		
			if (comparacion == 0) {
				return medio+1; 	// Indices base 1
			}
			if (comparacion < 0) {
				inicio = medio+1;
			} 
			else {
				fin = medio-1;
			}
		}
		return -(fin+2); 			// Indices base 1
	}
	
}
