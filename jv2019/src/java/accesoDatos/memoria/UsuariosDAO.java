/** 
 * Proyecto: Juego de la vida.
 * Resuelve todos los aspectos del almacenamiento de objetos Usuario utilizando un ArrayList y un Map - Hashtable.
 * Aplica el patrón Singleton.
 * Participa en el patrón Template-Method para el método indexSort().
 * Participa en el patrón Façade
 * @since: prototipo 0.2.0
 * @source: UsuariosDAO.java 
 * @version: 0.2.0 - 2020/04/20 
 * @author: ajp - Grupo 0
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import config.Configuracion;
import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Nif;
import modelo.Usuario;
import util.Fecha;

public class UsuariosDAO extends IndexSortTemplate implements OperacionesDAO {

	// Singleton. 
	private static UsuariosDAO instance;

	// Elementos de almacenamiento.
	private List<Identificable> datosUsuarios;
	private Map <String,String> equivalenciasId;

	/**
	 *  Método estático de acceso a la instancia única.
	 *  Si no existe la crea invocando al constructor interno.
	 *  Utiliza inicialización diferida.
	 *  Sólo se crea una vez; instancia única -patrón singleton-
	 *  @return instance
	 */
	public static UsuariosDAO getInstance() {
		if (instance == null) {
			instance = new UsuariosDAO();
		}
		return instance;
	}
	
	/**    
	 * Constructor por defecto de uso interno.
	 * Sólo se ejecutará una vez.
	 */
	private UsuariosDAO()  {
		datosUsuarios = new ArrayList <Identificable>();
		equivalenciasId = new Hashtable <String, String>();
		cargarUsuariosIntegrados();
	}

	private void cargarUsuariosIntegrados() {

		Usuario admin = null;
		try {
			Usuario invitado = new Usuario();
			alta(invitado);

			admin = new Usuario(new Nif(Configuracion.get().getProperty("usuario.adminNif")),
					Configuracion.get().getProperty("usuario.admin"), 
					Configuracion.get().getProperty("usuario.admin") + " " + Configuracion.get().getProperty("usuario.admin"),
					new DireccionPostal(),
					new Correo(Configuracion.get().getProperty("usuario.admin") + Configuracion.get().getProperty("correo.dominioPredeterminado")),
					new Fecha().addYears(-Integer.parseInt(Configuracion.get().getProperty("usuario.edadMinima"))),
					new Fecha(),
					new ClaveAcceso(Configuracion.get().getProperty("usuario.adminClaveAcceso")),
					Usuario.RolUsuario.ADMIN
					);
			alta(admin);
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		} 
		catch (DatosException e) {
			e.printStackTrace();
		}	
	}

	// OPERACIONES DAO
	
	/**
	 * Obtiene un Usuario dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	@Override
	public Usuario consultar(String id) {
		String idEquival = equivalenciasId.get(id);
		if (idEquival != null) {
			id = idEquival;
		}
		int index = indexSort(datosUsuarios, id);
		if (index < 0) {
			return null;
		}
		return (Usuario) datosUsuarios.get(index-1);
	}

	/**
	 * obtiene todas los usuarios en una lista.
	 * @return - la lista.
	 */
	@Override
	public List<Identificable> consultarTodos() {
		return datosUsuarios;
	}

	/**
	 * Registro de nuevo usuario.
	 * @param usr 
	 * @throws DatosException 
	 */
	@Override
	public void alta(Object obj) throws DatosException {
		assert obj != null;
		Usuario usr = (Usuario) obj;									// Para conversión cast.
		int posInsercion = indexSort(datosUsuarios, usr.getId());

		if (posInsercion < 0) {
			datosUsuarios.add(Math.abs(posInsercion)-1 , usr);  		// Inserta en orden.
			altaEquivalId(usr);
		}
		else {
			if (!datosUsuarios.get(posInsercion-1).equals(usr)) {
				producirVariantesIdUsr(usr);
			}
			else {
				throw new DatosException("UsuariosDAO.alta:" + usr.getId() +" Ya existe.");
			}
		}
	}
	
	/**
	 *  Si hay coincidencia de identificador hace 23 intentos de variar la última letra
	 *  procedente del NIF. Llama al generarVarianteIdUsr() de la clase Usuario.
	 * @param usrNuevo
	 * @param posicionInsercion
	 * @throws DatosException
	 */
	private void producirVariantesIdUsr(Usuario usr) throws DatosException {
		int posInsercion;
		// Coincidencia de id. Hay que generar variante.
		int intentos = "ABCDEFGHJKLMNPQRSTUVWXYZ".length();
		do {
			// Generar variante y comprueba de nuevo.
			usr = new Usuario(usr);	
			posInsercion = indexSort(datosUsuarios, usr.getId());
			if (posInsercion < 0) {
				datosUsuarios.add(-posInsercion - 1, usr); 		// Inserta el usuario en orden.
				altaEquivalId(usr);
				return;
			}
			intentos--;
		} while (intentos >= 0);
		throw new DatosException("UsuariosDAO.alta: imposible generar variante del " + usr.getId());
	}

	/**
	 *  Añade nif y correo como equivalencias de idUsr para el inicio de sesión. 
	 *	@param usr - Usuario a registrar equivalencias. 
	 */
	private void altaEquivalId(Usuario usr) {
		equivalenciasId.put(usr.getId(), usr.getId());
		equivalenciasId.put(usr.getNif().getTexto(), usr.getId());
		equivalenciasId.put(usr.getCorreo().getTexto(), usr.getId());	
	}

	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param id - el identificador del objeto a eliminar.
	 * @return - el Objeto eliminado.
	 * @throws DatosException - si no existe.
	 */
	@Override
	public Usuario baja(String id) throws DatosException {
		int index = indexSort(datosUsuarios, id);
		if (index > 0) {
			Usuario usr = (Usuario) datosUsuarios.get(index-1);
			// index es en base 1
			datosUsuarios.remove(index-1);
			bajaEquivalId(usr);
			return usr;
		}
		throw new DatosException("Datos.bajaUsuario: el usuario no existe...");	
	}
	
	private void bajaEquivalId(Usuario usr) {
		equivalenciasId.remove(usr.getId());
		equivalenciasId.remove(usr.getNif().getTexto());
		equivalenciasId.remove(usr.getCorreo().getTexto());		
	}
	
	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 *  @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	@Override
	public Usuario actualizar(Object obj) throws DatosException {
		assert obj != null;
		Usuario usr = (Usuario) obj;
		int index = indexSort(datosUsuarios, usr.getId());
		if (index > 0) {
			Usuario aux = (Usuario) datosUsuarios.get(index-1);
			// index es en base 1
			datosUsuarios.set(index-1, usr);
			bajaEquivalId(aux);
			altaEquivalId(usr);
			return aux;
		}
		throw new DatosException("UsuariosDAO.actualizar: el usuario no existe...");
	}

	/**
	 * Obtiene el listado de todos los usuarios almacenados.
	 * @return el texto con el volcado de datos.
	 */
	@Override
	public String toStringDatos() {
		StringBuilder result = new StringBuilder();
		for (Identificable usr: datosUsuarios) {
			result.append("\n" + usr); 
		}
		return result.toString();
	}

	/**
	 * Obtiene el listado de todos id de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	@Override
	public String toStringIds() {
		StringBuilder result = new StringBuilder();
		for (Identificable usr: datosUsuarios) {
			result.append("\n" + usr.getId()); 
		}
		return result.toString();
	}
	
	/**
	 * Elimina todos los usuarios almacenados y regenera los predeterminados.
	 */
	@Override
	public void borrarTodo() {
		datosUsuarios.clear();
		equivalenciasId.clear();
		cargarUsuariosIntegrados();
	}

} 