/** 
 * Proyecto: Juego de la vida.
 * Resuelve todos los aspectos del almacenamiento de objetos Sesion utilizando un ArrayList.
 * Aplica el patrón Singleton.
 * Participa en el patrón Template-Method para el método indexSort().
 * Participa en el patrón Façade.
 * @since: prototipo 0.2.0
 * @source: SesionesDAO.java 
 * @version: 0.2.0 - 2020/04/20 
 * @author: ajp - Grupo 0
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Identificable;
import modelo.Sesion;
import util.Fecha;

public class SesionesDAO extends IndexSortTemplate implements OperacionesDAO {

	// Singleton.
	private static SesionesDAO instance;

	// Elemento de almacenamiento. 
	private ArrayList<Identificable> datosSesiones;

	/**
	 * Constructor por defecto de uso interno.
	 * Sólo se ejecutará una vez.
	 */
	private SesionesDAO() {
		datosSesiones = new ArrayList<Identificable>();
	}

	/**
	 *  Método estático de acceso a la instancia única.
	 *  Si no existe la crea invocando al constructor interno.
	 *  Utiliza inicialización diferida.
	 *  Sólo se crea una vez; instancia única -patrón singleton-
	 *  @return instance
	 */
	public static SesionesDAO getInstance() {
		if (instance == null) {
			instance = new SesionesDAO();
		}
		return instance;
	}

	//OPERACIONES DAO
	/**
	 * Búsqueda de sesión por idSesion.
	 * @param id - el idUsr+fecha a buscar.
	 * @return - la sesión encontrada; sin encuentra. 
	 */
	@Override
	public Sesion consultar(String id) {
		assert id != null;
		int posicion = indexSort(datosSesiones, id);			 // En base 1
		if (posicion >= 0) {
			return (Sesion) datosSesiones.get(posicion - 1);     // En base 0
		}
		return null;
	}

	/**
	 * obtiene todas las sesiones en una lista.
	 * @return - la lista.
	 */
	@Override
	public List<Identificable> consultarTodos() {
		return datosSesiones;
	}

	/**
	 * Búsqueda de todas la sesiones de un mismo usuario.
	 * @param idUsr - el identificador de usuario a buscar.
	 * @return - Sublista con las sesiones encontrada.
	 */
	public List<Identificable> consultarTodasUsuario(String idUsr) {
		assert idUsr != null;
		String idSesion = idUsr + ":" + new Fecha().toStringMarcaTiempo();
		//Busca posición inserción ordenada por idUsr + fecha. La última para el mismo usuario.
		int index = indexSort(datosSesiones, idSesion);
		if (index > 0) {
			return filtrarSesionesUsr(indexSort(datosSesiones, idSesion) - 1);
		}
		return null;
	}

	/**
	 * Separa en una lista independiente de todas las sesiones de un mismo usuario.
	 * @param ultima - el indice de una sesion almacenada.
	 * @return - Sublista con las sesiones encontrada; null si no existe ninguna.
	 */
	private List<Identificable> filtrarSesionesUsr(int ultima) {
		String idUsr = ((Sesion) datosSesiones.get(ultima)).getUsr().getId();
		int primera = ultima;
		// Localiza primera sesión del mismo usuario.
		for (int i = ultima; i >= 0 && ((Sesion) datosSesiones.get(i)).getUsr().getId().equals(idUsr); i--) {
			primera = i;
		}
		// devuelve la sublista de sesiones buscadas.
		return datosSesiones.subList(primera, ultima+1);
	}

	/**
	 * Alta de una nueva SesionUsuario en orden y sin repeticiones según IdUsr + fecha. 
	 * Busca previamente la posición que le corresponde por búsqueda binaria.
	 * @param obj - la SesionUsuario a almacenar.
	 * @throws DatosException - si ya existe.
	 */
	@Override
	public void alta(Object obj) throws DatosException  {
		assert obj != null;
		Sesion sesionNueva = (Sesion) obj;							// Para conversión cast
		int posInsercion = indexSort(datosSesiones, sesionNueva.getId()); 
		if (posInsercion < 0) {
			datosSesiones.add(Math.abs(posInsercion)-1, sesionNueva); 				// Inserta la sesión en orden.
		}
		else {
			throw new DatosException("SesionesDAO.alta: "+ sesionNueva.getId() + " ya existe");
		}
	}

	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param idSesion - identificador de la SesionUsuario a eliminar.
	 * @return - la SesionUsuario eliminada.
	 * @throws DatosException - si no existe.
	 */
	@Override
	public Sesion baja(String idSesion) throws DatosException  {
		assert idSesion != null;
		int posicion = indexSort(datosSesiones, idSesion); 							// En base 1
		if (posicion > 0) {
			return (Sesion) datosSesiones.remove(posicion - 1); 				    // En base 0
		}
		else {
			throw new DatosException("SesionesDAO.baja: "+ idSesion + " no existe");
		}
	}

	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 *  @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	@Override
	public Sesion actualizar(Object obj) throws DatosException {
		assert obj != null;
		Sesion sesionActualizada = (Sesion) obj;							// Para conversión cast
		int index = indexSort(getInstance().datosSesiones, sesionActualizada.getId());
		if (index > 0) {
			Sesion aux = (Sesion) datosSesiones.get(index-1);
			/// index es en base 1
			datosSesiones.set(index-1, sesionActualizada);
			return aux;
		}
		throw new DatosException("SesionesDAO.actualizar: la sesión no existe...");
	}

	/**
	 * Obtiene el listado de todos las sesiones almacenadas.
	 * @return el texto con el volcado de datos.
	 */
	@Override
	public String toStringDatos() {
		StringBuilder result = new StringBuilder();
		for (Identificable sesiones: datosSesiones) {
			result.append("\n" + sesiones);
		}
		return result.toString();
	}

	/**
	 * Obtiene el listado de todos id de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	@Override
	public String toStringIds() {
		StringBuilder result = new StringBuilder();
		for (Identificable sesiones: datosSesiones) {
			result.append("\n" + sesiones.getId()); 
		}
		return result.toString();
	}

	/**
	 * Elimina todos las sesiones almacenadas.
	 */
	@Override
	public void borrarTodo() {
		datosSesiones.clear();	
	}

	/**
	 * Total de sesiones almacenadas.
	 */
	public int totalRegistrado() {
		return datosSesiones.size();
	}

}
