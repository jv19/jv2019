/** 
 * Proyecto: Juego de la vida.
 * Resuelve todos los aspectos del almacenamiento de objetos Simulacion utilizando un ArrayList.
 * Aplica el patrón Singleton.
 * Participa en el patrón Template-Method para el método indexSort().
 * Participa en el patrón Façade.
 * @since: prototipo 0.2.0
 * @source: SimulacionesDAO.java 
 * @version: 0.2.0 - 2020/04/20
 * @author: ajp - Grupo 0
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Sesion;
import modelo.Simulacion;
import util.Fecha;

public class SimulacionesDAO extends IndexSortTemplate implements OperacionesDAO {

	// Singleton.
	private static SimulacionesDAO instance;

	// Elemento de almacenamiento.
	private ArrayList <Identificable> datosSimulaciones;

	/**
	 *  Método estático de acceso a la instancia única.
	 *  Si no existe la crea invocando al constructor interno.
	 *  Utiliza inicialización diferida.
	 *  Sólo se crea una vez; instancia única -patrón singleton-
	 *  @return instance 
	 */
	public static SimulacionesDAO getInstance() {
		if (instance == null) {
			instance = new SimulacionesDAO();
		}
		return instance;
	}
	
	/**
	 * Constructor por defecto de uso interno.
	 * Sólo se ejecutará una vez.
	 */
	private SimulacionesDAO() {
		datosSimulaciones = new ArrayList <Identificable>();
		cargarSimulacionDemo();
	}

	private void cargarSimulacionDemo() {
		try {
			alta(new Simulacion());
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		}
	}

	// OPERACIONES DAO
	/**
	 * Búsqueda de Simulacion dado idUsr y fecha.
	 * @param id - el idUsr+fecha de la Simulacion a buscar. 
	 * @return - la Simulacion encontrada; null si no encuentra. 
	 */	
	@Override
	public Simulacion consultar(String id) {
		assert id != null;
		int posicion = indexSort(datosSimulaciones, id);				// En base 1
		if (posicion >= 0) {
			return (Simulacion) datosSimulaciones.get(posicion - 1);    // En base 0
		}
		return null;
	}

	/**
	 * obtiene todas las simulaciones en una lista.
	 * @return - la lista.
	 */
	@Override
	public List consultarTodos() {
		return datosSimulaciones;
	}

	/**
	 * Búsqueda de todas la simulaciones de un usuario.
	 * @param idUsr - el identificador de usuario a buscar.
	 * @return - Sublista con las simulaciones encontrada; null si no existe ninguna.
	 * @throws ModeloException 
	 */
	public List<Identificable> consultarTodasUsuario(String idUsr) {
		assert idUsr != null;
		String idSimulacion = idUsr + "-" + new Fecha().toStringMarcaTiempo();
		//Busca posición inserción ordenada por idUsr + fecha. La última para el mismo usuario.
		int index = indexSort(datosSimulaciones, idSimulacion);
		if (index > 0) {
			return filtrarSimulacionesUsr(indexSort(datosSimulaciones, idSimulacion) - 1);
		}
		return null;
	}

	/**
	 * Separa en una lista independiente todas la simulaciones de un mismo usuario.
	 * @param ultima - el indice de la última simulación ya encontrada.
	 * @return - Sublista con las simulaciones encontrada; null si no existe ninguna.
	 */
	private List<Identificable> filtrarSimulacionesUsr(int ultima) {
		String idUsr = ((Sesion) datosSimulaciones.get(ultima)).getUsr().getId();
		int primera = ultima;
		// Localiza primera simulación del mismo usuario.
		for (int i = ultima; i >= 0 && ((Sesion) datosSimulaciones.get(i)).getUsr().getId().equals(idUsr); i--) {
			primera = i;
		}
		// devuelve la sublista de simulaciones buscadas.
		return datosSimulaciones.subList(primera, ultima+1);
	}

	/**
	 *  Alta de una nueva Simulacion en orden y sin repeticiones según los idUsr más fecha. 
	 *  Busca previamente la posición que le corresponde por búsqueda binaria.
	 *  @param obj - Simulación a almacenar.
	 * @throws DatosException - si ya existe.
	 */	
	public void alta(Object obj) throws DatosException  {
		assert obj != null;
		Simulacion simulacion = (Simulacion) obj;								// Para conversión cast
		int posInsercion = indexSort(datosSimulaciones, simulacion.getId()); 
		if (posInsercion < 0) {
			datosSimulaciones.add(Math.abs(posInsercion)-1, simulacion); 		// Inserta la simulación en orden.
		}
		else {
			throw new DatosException("SimulacionesDAO.alta: "+ simulacion.getId() + " ya existe");
		}
	}

	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param idSimulacion - identificador de la Simulacion a eliminar.
	 * @return - la Simulacion eliminada. 
	 * @throws DatosException - si no existe.
	 */
	@Override
	public Simulacion baja(String idSimulacion) throws DatosException  {
		assert (idSimulacion != null);
		int posicion = indexSort(datosSimulaciones, idSimulacion); 								// En base 1
		if (posicion > 0) {
			return (Simulacion) datosSimulaciones.remove(posicion - 1); 						// En base 0
		}
		else {
			throw new DatosException("SimulacionesDAO.baja: "+ idSimulacion + " no existe");
		}
	}
	
	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 *  @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	@Override
	public Simulacion actualizar(Object obj) throws DatosException {
		assert obj != null;
		Simulacion simulActualizada = (Simulacion) obj;							// Para conversión cast
		int index = indexSort(datosSimulaciones, simulActualizada.getId());
		if (index > 0) {
			Simulacion aux = (Simulacion) datosSimulaciones.get(index-1);
			/// index es en base 1
			datosSimulaciones.set(index-1, simulActualizada);
			return aux;
		}
		throw new DatosException("SimulacionesDAO.actualizar: la simulación no existe...");
	}
	
	/**
	 * Obtiene el listado de todos las simulaciones almacenadas.
	 * @return el texto con el volcado de datos.
	 */
	@Override
	public String toStringDatos() {
		StringBuilder result = new StringBuilder();
		for (Identificable simulacion: datosSimulaciones) {
			result.append("\n" + simulacion);
		}
		return result.toString();
	}

	/**
	 * Obtiene el listado de todos id de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	@Override
	public String toStringIds() {
		StringBuilder result = new StringBuilder();
		for (Identificable simulacion: datosSimulaciones) {
			result.append("\n" + simulacion.getId()); 
		}
		return result.toString();
	}

	/**
	 * Elimina todos las simulaciones almacenadas y regenera la demo predeterminada.
	 */
	@Override
	public void borrarTodo() {
		datosSimulaciones.clear();
		cargarSimulacionDemo();
	}

} 
