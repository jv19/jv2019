/** 
 *  Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 	
 * @since: prototipo 0.1.0
 * @source: JVida.java 
 * @version: 0.2.0 - 2020/06/01
 * @author: ajp - Grupo 0
 */

package _main;

import accesoUsr.control.consola.ControlPrincipal;

public class JVida {
	
	public static void main(String[] args) {		
		if (args.length > 0) {
			new ControlPrincipal(args[0]);  // Id de usuario directo en linea comandos.
			return;
		}
		new ControlPrincipal();
	}

} 
