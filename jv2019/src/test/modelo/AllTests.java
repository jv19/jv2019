/** 
 * Proyecto: Juego de la vida.
 *  Prueba Junit5 del paquete modelo según el modelo 1.1
 *  @since: prototipo 0.1.0
 *  @source: AllTest.java 
 *  @version: 0.1.1 - 2020/02/10
 *  @author: ajp
 */

package modelo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	ClaveAcceso.class,
	CorreoTest.class,
	DireccionPostalTest.class,
	NifTest.class,
	SesionTest.class,
	SimulacionTest.class,
	UsuarioTest.class
})

public class AllTests {

}
