/** 
 *  Proyecto: Juego de la vida.
 *  Clase JUnit para pruebas del DAO de objetos Usuario y la parte de la fachada correspondiente.
 *  @since: prototipo 0.2.0
 *  @source: UsuariosDAO.java 
 *  @version: 0.2.0 - 2020/04/29 
 *  @author: ajp - Grupo 0
 */

package accesoDatos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Nif;
import modelo.Usuario;
import modelo.Usuario.RolUsuario;
import util.Fecha;

public class UsuariosDAOTest {

	private static DatosFacade datos;
	private Usuario usrPrueba;

	/**
	 * Método que se ejecuta una sola vez al principio del conjunto pruebas. 
	 */
	@BeforeAll
	public static void crearFachadaDatos() {
		datos = new DatosFacade();
	}

	/**
	 * Método que se ejecuta antes de cada @test.
	 */
	@BeforeEach
	public void crearDatosPrueba() {
			// Usuario con id "PMA8P"
			usrPrueba =  new Usuario(new Nif("00000008P"), "Pepe",
					"Márquez Alón", new DireccionPostal("Alta", "10", "30012", "Murcia"), 
					new Correo("pepe@gmail.com"), new Fecha(1990, 11, 12), 
					new Fecha(2018, 02, 05), new ClaveAcceso("Miau#32"), RolUsuario.NORMAL);
	}

	/**
	 * Método que se ejecuta al terminar de cada @test.
	 */
	@AfterEach
	public void borraDatosPrueba() {
		datos.borrarTodosUsuarios();
		usrPrueba = null;
	}

	@Test
	public void testConsultar() {	
		datos.altaUsuario(usrPrueba);
		// El mismo Usuario de la prueba.
		assertSame(usrPrueba, datos.consultarUsuario(usrPrueba.getId()));
	}

	@Test
	public void testConsultarPredeterminados() {
		// Predeterminados del sistema...
		assertEquals(datos.consultarUsuario("III0T").getId(), "III0T");
		assertEquals(datos.consultarUsuario("AAA1R").getId(), "AAA1R");
	}

	@Test
	void testConsultarTodos() {
		datos.altaUsuario(usrPrueba);
		// Hay dos elementos predeterminados más uno...
		assertEquals(datos.consultarTodosUsuario().size(), 3);
	}
	
	@Test
	public void testAlta() {
		// Usuario nuevo, que no existe.
		datos.altaUsuario(usrPrueba);
		// El mismo Usuario de la prueba.
		assertSame(usrPrueba, datos.consultarUsuario(usrPrueba.getId()));
	}

	@Test
	public void testBaja() {
		datos.altaUsuario(usrPrueba);
		// Baja del mismo Usuario almacenado.
		assertSame(usrPrueba, datos.bajaUsuario(usrPrueba));
	}

	@Test
	public void testActualizar() {
		// Usuario nuevo.
		datos.altaUsuario(usrPrueba);
		usrPrueba.setApellidos("Ramírez Pinto");
		datos.actualizarUsuario(usrPrueba);
		assertEquals(datos.consultarUsuario(usrPrueba.getId()).getApellidos(), "Ramírez Pinto");
	}

	@Test
	public void testToStringDatos() {
		assertNotNull(datos.toStringDatosUsuarios());
	}

	@Test
	public void testToStringIds() {
		assertNotNull(datos.toStringIdsUsuarios());
	}

	@Test
	void testBorrarTodo() {
		datos.altaUsuario(usrPrueba);
		// Hay dos elementos predeterminados más uno...
		datos.borrarTodosUsuarios();
		// Predeterminados del sistema...
		assertEquals(datos.consultarUsuario("III0T").getId(), "III0T");
		assertEquals(datos.consultarUsuario("AAA1R").getId(), "AAA1R");
	}
	
	@Test
	public void testGetEquivalenciaId() {		
		// Usuario nuevo...
		datos.altaUsuario(usrPrueba);
		assertEquals(datos.consultarUsuario("PMA8P").getId(), "PMA8P");
		assertEquals(datos.consultarUsuario("00000008P").getId(), "PMA8P");
		assertEquals(datos.consultarUsuario("pepe@gmail.com").getId(), "PMA8P");
	}

	// Con datos anómalos
	/////////////////////
	
	@Test
	public void testConsultarUsuarioNoExiste() {	
		// Usuario no existe.
		assertNull(datos.consultarUsuario(usrPrueba.getId()));
	}

	@Test
	public void testAltaUsuarioRepetido() {
		try {
			// Usuario nuevo.
			datos.altaUsuario(usrPrueba);
			// Usuario repetido.
			datos.altaUsuario(usrPrueba);
			fail("No debe llegar aquí...");
		}
		catch (DatosException e) {
		}
	}

	@Test
	public void testBajaUsuarioNoExiste() {
		try {
			// Usuario no existe.
			datos.bajaUsuario(usrPrueba);
			fail("No debe llegar aquí...");
		}
		catch (DatosException e) {
		}
	}

	@Test
	public void testActualizarUsuarioNoExiste() {
		try {
			// Usuario no existe.
			usrPrueba.setApellidos("Ramírez Pinto");
			datos.actualizarUsuario(usrPrueba);
			fail("No debe llegar aquí...");
		}
		catch (DatosException e) {
		}
	}

} 
